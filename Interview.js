const fs = require('fs')

const file = fs.readFileSync('wiki.txt', 'utf8', (err, data) => {
    if (err) throw console.log(err)
    return data
});

var words = [];

words = file.split(' ');

const list = words.map(word => {
    let wordList = {};

    wordList.word = word;
    wordList.score = 0;

    return wordList;
})

function returnWordsScore() {
    let i = 0;

    while (i < list.length) {
        for (let j = 0; j < list.length;) {
            if (list[i].word === words[j]) {
                list[i].score++;
                j++;
            }
            j++
        }
        i++;
    }

    const max = list.reduce((a, b) => a.score > b.score ? a : b);
    
    for (let wordCount in Object.values(list)) {
        list[wordCount].score = list[wordCount].score*2/max.score;
    }

    const ordered = list.sort((a, b) => b.score - a.score);

    ordered.forEach(word => console.log("Word: " + word.word + "    Score: " + word.score.toFixed(1)));
}

returnWordsScore();